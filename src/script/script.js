function initSlideshow() {
  // get carousel container
  const $container = $('.carousel--container');
  // get carousel items
  const $items = $container.find('.carousel--item');
  let activeIndex = 0;
  const maxIndex = $items.length - 1;

  const $leftArrow = $container.find('.carousel--arrow__left');
  const $rightArrow = $container.find('.carousel--arrow__right');

  function setContainerHeight() {
    let maxHeight = 0;
    $items.each(function iteration() {
      const $this = $(this);
      const height = $this.height();
      if (height > maxHeight) {
        maxHeight = height;
      }
    });
    $container.height(maxHeight);
  }
  function setActiveSlideByIndex(index) {
    let nextIndex = index;
    if (index < 0) {
      nextIndex = maxIndex;
    }
    if (index > maxIndex) {
      nextIndex = 0;
    }
    $items.eq(activeIndex).removeClass('carousel--item__active');
    $items.eq(nextIndex).addClass('carousel--item__active');

    return nextIndex;
  }

  function nextSlide() {
    activeIndex = setActiveSlideByIndex(activeIndex + 1);
  }

  function prevSlide() {
    activeIndex = setActiveSlideByIndex(activeIndex - 1);
  }

  // set 'slide' class for items
  $items.addClass('carousel--item__slide');
  // set 'active' class for first item (activeIndex = 0)
  $items.eq(activeIndex).addClass('carousel--item__active');
  // set height for container
  setContainerHeight($container, $items);
  // set height for container after window is loaded or resized
  $(window).on('load resize', () => {
    setContainerHeight($container, $items);
  });

  $leftArrow.on('click', prevSlide);
  $rightArrow.on('click', nextSlide);

  $container.on('swipeleft', prevSlide);
  $container.on('swiperight', nextSlide);
}

$(() => {
  $.mobile.loading().hide();

  initSlideshow();
});
