module.exports = {
    "extends": "airbnb-base",
    // "installedESLint": true,
    "plugins": [
      "jquery"
    ],
    "env": {
        "browser": true,
        "jquery": true,
    },
    "rules":{
      "arrow-body-style": ["error", "always"]
    }
};
