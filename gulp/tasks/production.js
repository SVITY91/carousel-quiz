import gulp from 'gulp';
import useref from 'gulp-useref';
import gulpif from 'gulp-if';
import uglify from 'gulp-uglify';
import cleanCss from 'gulp-clean-css';

// Release
gulp.task('production', () => {
  gulp.src('./public/*.html')
    .pipe(useref())
    .pipe(gulpif('*.js', uglify()))
    .pipe(gulpif('*.css', cleanCss()))
    .pipe(gulp.dest('./public'));
});
