import gulp from 'gulp';
import plumber from 'gulp-plumber';
import notify from 'gulp-notify';
import babel from 'gulp-babel';

//
// JS
//
gulp.task('script', () => {
  gulp.src(['src/script/*.js'])
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>'),
    }))
    .pipe(babel({
      presets: ['es2015'],
    }))
    .pipe(gulp.dest('public/script'));
});
