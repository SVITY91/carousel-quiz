import gulp from 'gulp';
import plumber from 'gulp-plumber';
import notify from 'gulp-notify';
import postcss from 'gulp-postcss';

//
// PostCSS
//
gulp.task('postcss', () => {
  const processors = [
    require('precss'),
    require('lost'),
    require('postcss-calc')({
      mediaQueries: true,
    }),
    require('postcss-short'),
    require('postcss-assets')({
      loadPaths: ['src/images/'],
      // baseUrl: 'images/'
      relative: true,
    }),
    require('postcss-cssnext')({
      browsers: ['>0.1%', 'ie >= 8'],
    }),
    require('precss'),
    require('postcss-filter-gradient'),
  ];

  return gulp.src('src/postcss/**/*.css')
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>'),
    }))
    .pipe(postcss(processors))
    .pipe(gulp.dest('public/css'));
});
