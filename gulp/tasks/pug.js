import gulp from 'gulp';
import plumber from 'gulp-plumber';
import notify from 'gulp-notify';
import pug from 'gulp-pug';

//
// Jade Templates
//
gulp.task('pug', () => {
  gulp.src(['src/pug/*.pug', '!src/pug/_*.pug'])
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>'),
    }))
    .pipe(pug({
      pretty: true,
    }))
    .pipe(gulp.dest('public'));
});
