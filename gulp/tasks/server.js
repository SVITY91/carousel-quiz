import gulp from 'gulp';
import connect from 'gulp-connect';
import ip from 'ip';

const PORT = process.env.PORT || 8080;
const HOST = process.env.HOST || ip.address() || '127.0.0.1';

//
//  Server
//
gulp.task('server', () => {
  connect.server({
    root: './public/',
    port: PORT,
    host: HOST,
  });
});
