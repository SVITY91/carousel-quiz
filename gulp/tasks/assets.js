import gulp from 'gulp';
import plumber from 'gulp-plumber';
import notify from 'gulp-notify';

//
// Image
//
gulp.task('assets', () => {
  gulp.src('src/images/**/*.*')
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>'),
    }))
    .pipe(gulp.dest('public/images'));

  gulp.src(['src/*.*', '!src/*.html'])
    .pipe(gulp.dest('public'));
});
