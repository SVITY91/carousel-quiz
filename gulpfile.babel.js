import gulp from 'gulp';
import gulpSequence from 'gulp-sequence';
import requireDir from 'require-dir';


requireDir('./gulp/tasks');

gulp.task('build', (cb) => {
  gulpSequence('assets', 'postcss', 'script', 'pug')(cb);
});

gulp.task('release', (cb) => {
  gulpSequence(['assets', 'postcss', 'script', 'pug'], 'production')(cb);
});

gulp.task('default', ['build', 'server', 'watch']);

gulp.task('watch', () => {
  gulp.watch('src/postcss/**/*.css', ['postcss']);
  gulp.watch('src/script/**/*.js', ['script']);
  gulp.watch('src/images/**/*.*', ['assets']);
  gulp.watch('src/pug/**/*.pug', ['pug']);
});
