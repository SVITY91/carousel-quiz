#
# GULP
#

# build
npm i -S gulp-sequence require-dir
npm i -S gulp gulp-babel gulp-connect gulp-pug
# Errors notifications
npm i -S gulp-notify gulp-plumber

#
# POSTCSS
#
npm i -S gulp-postcss lost postcss-assets postcss-calc postcss-cssnext postcss-short postcss-triangle precss

# release
npm i -S gulp-useref gulp-clean-css gulp-uglify gulp-if

#
# ESLINT
#
npm i -D eslint-config-airbnb-base eslint eslint-plugin-import eslint-plugin-jquery

#
# BABEL
#
npm i -S babel-core babel-preset-es2015
# babel-core for webpack & gulp(using es6)

#
# Bower
#

bower install --save jquery#^1.12.4 html5shiv respond normalize-css
